function sum(a, b) {
  return a + b;
}
const pow = (a,b) => {
    let sum = a;
    b--;
    while (b > 0) {
        sum *= a;
        b--;
    }
    return sum;
}
module.exports = {sum, pow};