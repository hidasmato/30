const { sum, pow } = require('./func');

//Верно
test('Складываем 1 и 2, получаем 3', () => {
    expect(sum(1, 2)).toBe(3);
});

//Верно
test('2 в 3 степени рано 8', () => {
    expect(pow(2, 3)).toBe(8);
});

//Неверно (2^5 равно 32)
test('2 в 5 степени рано 16', () => {
    expect(pow(2, 3)).toBe(16);
});